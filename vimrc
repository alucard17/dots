"""""""""""""""""""""""""""""""""""""""""""""""""
"                                               "
"   by Alucard17, alucard17@gitlab              "
"   (my vimrc, using vundle as package manager)  "
"                                               "
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""
" Vundle packet manager
"""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'flazz/vim-colorschemes'
Plugin 'ap/vim-css-color'
Plugin 'vim-python/python-syntax'
Plugin 'lervag/vimtex'
Plugin 'vim-syntastic/syntastic'
Plugin 'preservim/nerdtree'
Plugin 'mattn/emmet-vim'
Plugin 'Yggdroot/indentLine'
Plugin 'yuezk/vim-js'
Plugin 'maxmellon/vim-jsx-pretty'
Plugin 'tpope/vim-commentary'
Plugin 'dense-analysis/ale'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'reedes/vim-pencil'
Plugin 'junegunn/goyo.vim'
Plugin 'elzr/vim-json'
Plugin 'JamshedVesuna/vim-markdown-preview'

call vundle#end()
filetype plugin indent on

""""""""""""""""""""""""""""""""""""""""""""""""
" General settings
""""""""""""""""""""""""""""""""""""""""""""""""
syntax on
set number
set relativenumber

colorscheme anderson
let g:airline_theme='deus'

" Tabs to space
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" Plugin setting
"" airline stuff
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'

"" python-syntax
let g:python_highlight_all = 1

"" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"" vimtex
let g:tex_flavor = "latex"
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_toc_config = {
                \'split_pos'   : ':vert :botright',
                \'split_width':  35,
                \'layers' : ['content'],
                \'fold_enable' : 1,
                \'show_help' : 0,
                \}
let g:vimtex_fold_enabled = 0
let g:vimtex_fold_types = {
                \'comments' : {'enabled' : 1},
                \'envs' : {
                \'blacklist' : [],
                \'whitelist' : ['figure', 'table'],
                \},
                \}

set nofoldenable		"Disable folding when opening a file

"" nerdtree
nnoremap <leader>t :NERDTreeToggle<cr>
highlight! link NERDTreeFlags NERDTreeDir "Apply highliting to folders icons
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1
let g:DevIconsEnableFolderExtensionPatternMatching = 1
let NERDTreeDirArrowExpandable=""
let NERDTreeDirArrowCollapsible=""
let g:NERDTreeMinimalUI = v:true

"" emmet-vim
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

"" vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1

"" vim-json
let g:vim_json_syntax_conceal = 0